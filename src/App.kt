import kotlinx.cinterop.*
import csfml.*

fun main(args: Array<String>)
{
    val videoMode = VideoMode()
    val contextSettings = ContextSettings()
    val windowStyle = WindowStyle.DefaultStyle

    val window = RenderWindow(videoMode, "Hello", windowStyle, contextSettings)
}

class RenderWindow(val videoMode: VideoMode, val title: String, val windowStyle: WindowStyle, val contextSettings: ContextSettings)
{
    val nativePtr: NativePtr

    init
    {
        nativePtr = sfRenderWindow_create(videoMode.ptr.value, title, windowStyle.id, contextSettings.ptr)
    }
}

class VideoMode()
{
    val width: Int = 800
    val height: Int = 600
    val bitsPerPixel: Int = 32

    val ptr = nativeHeap.alloc<sfVideoMode>()

    init
    {
        ptr.width = width
        ptr.height = height
        ptr.bitsPerPixel = bitsPerPixel
    }
}

class ContextSettings
{
    val ptr = nativeHeap.alloc<sfContextSettings>()
}

enum class WindowStyle(val id: Int)
{
    None(0),
    Titlebar(1),
    Resize(2),
    Close(4),
    Fullscreen(8),
    DefaultStyle(7)
}


/*

    val videoMode = nativeHeap.alloc<sfVideoMode>()
    videoMode.width = 800
    videoMode.height = 600
    videoMode.bitsPerPixel = 32

    val contextSettings = nativeHeap.alloc<sfContextSettings>()

    val window = sfRenderWindow_create(videoMode, "Hello", sfDefaultStyle, contextSettings)
 */